<?php
require_once 'utils/utils.php';
require_once 'utils/File.php';
require_once 'entity/Asociado.php';
require_once 'repository/AsociadoRepository.php';
require_once 'database/Connection.php';
require_once 'core/App.php';


$errores = [];
$nombre = '';
$descripcion = '';

$mensaje = '';

try {
    $config = require_once 'app/config.php';
    App::bind('config', $config);
    $AsociadoRepository = new AsociadoRepository();


    //alta de los asociados
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {


        $nombre = trim(htmlspecialchars($_POST['nombre']));

        $descripcion = trim(htmlspecialchars($_POST['descripcion']));

        $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];

        $logo = new File('logo', $tiposAceptados);
        $logo->saveUploadFile(Asociado::RUTA_LOGOS_ASOCIADOS);



        $asociado = new Asociado($nombre, $logo->getFileName(), $descripcion);
        $AsociadoRepository->save($asociado);
        $mensaje = 'se ja giardadp ña o,agem';
        //despies de guiardar resetamo sel parametro de la descriptn
        $descripcion = '';


    }

    //mostrar asociados
    $asociados = $AsociadoRepository->findAll();


} catch (PDOException2 $exception){
    throw new PDOException2('cuec pdo ' .$exception);
} catch (AppException $e) {
    throw new AppException('app ex ' . $e);
} catch (FileException $e) {
    throw new FileException('shet ' . $e);
}


require 'views/asociados.view.php';