<?php
/**
 * Created by PhpStorm.
 * User: 4710HQ
 * Date: 13/12/2018
 * Time: 12:13
 */

//include('C:/xampp/htdocs/php7.local/database/QueryBuilder.php');

require_once __DIR__ . '/../database/QueryBuilder.php';

class ImagenGaleriaRepository extends QueryBuilder
{

    public function __construct(string $table = 'imagenes', string $classEntity = 'ImagenGaleria')
    {
        parent::__construct($table, $classEntity);
    }
}