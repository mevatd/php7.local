<?php

require_once 'entity/ImagenGaleria.php';
require_once 'entity/Asociado.php';

$imagenes = [];
for ($i = 1; $i <= 12; $i++) {
    $imagenes[] = new ImagenGaleria($i . '.jpeg', 'descripcion ' . $i, 0, 0, 0);

}

require 'utils/utils.php';
require 'views/index.view.php';

