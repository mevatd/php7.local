<?php
/**
 * Created by PhpStorm.
 * User: lliurex
 * Date: 07/12/2018
 * Time: 10:33
 */


abstract class QueryBuilder
{
    private $connection;
    private $table;
    private $classEntity;


    /**
     * QueryBuilder constructor.
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    /**
     * @return array
     * @throws QueryException
     */
    public function findAll()
    {
        $sql = "SELECT * FROM $this->table";
        //$this->connection->query($sql);
        $pdostatement = $this->connection->prepare($sql);
        if ($pdostatement->execute() === false) {
            throw new QueryException("No se ha podido ejecutar la query");
        } else {
            return $pdostatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
        }
    }

    /**
     * @param IEntity $entity
     * @throws PDOException2
     */
    public function save(IEntity $entity)
    {
        try {
            $paramenters = $entity->toArray();

            $sql = sprintf('INSERT INTO %s (%s) VALUES (%s)',
                $this->table,
                implode(', ',array_keys($paramenters)),
                ':'.implode(', :', array_keys($paramenters))
            );

            $pdoStatement = $this->connection->prepare($sql);
            $pdoStatement->execute($paramenters);

        } catch (PDOException2 $exception) {
            throw new PDOException2('mec builder');
        }


    }
}